<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php if ( isset($title) ) : echo $title; else: echo 'Simple Image Upload'; endif; ?></title>

    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<style type="text/css">
	html {
	  position: relative;
	  min-height: 100%;
	}
	body {
	  margin-bottom: 60px;
	}
	.footer {
	  position: absolute;
	  bottom: 0;
	  width: 100%;
	  height: 60px;
	  background-color: #e8eaf6;
	}
	.container {
	  width: auto;
	  max-width: 680px;
	  padding: 0 15px;
	}
	.footer .developed {
	  margin: 20px 0;
	  font-size:smaller;
	}
	h1.center{
		text-align:center;
	}
	.navbar {
		background-color:#e8eaf6;
	}
	.navbar-default .navbar-nav>.active>a, .navbar-default .navbar-nav>.active>a:focus, .navbar-default .navbar-nav>.active>a:hover{
		background-color:#c5cae9;
	}
	.navbar-default .navbar-nav>li>a:focus, .navbar-default .navbar-nav>li>a:hover{
		background-color:#7986cb;
		color:#FFF;
	}
	p.center{
		text-align:center;
	}
	/* Extra small devices (phones, less than 768px) */
	/* No media query since this is the default in Bootstrap */

	/* Small devices (tablets, 768px and up) */
	@media (min-width: @screen-sm-min) { ... }

	/* Medium devices (desktops, 992px and up) */
	@media (min-width: @screen-md-min) { ... }

	/* Large devices (large desktops, 1200px and up) */
	@media (min-width: @screen-lg-min) { ... }
	<?php if ( isset($extra_css) ) echo $extra_css; ?>
	</style>
  </head>
  <body>
   <nav class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <div class="navbar-brand"><span class="glyphicon glyphicon-picture"></span> <span class="glyphicon glyphicon-cloud-upload" aria-hidden="true"></span> Simple Image Upload</div>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
		  <?php
		   //actively assign the 'active' class on current page detected
		   $current_page = Request::detect_uri(); 
		  ?>
            <li class="<?php if( $current_page=='/' ) echo 'active'; ?>"><a href="/">Home</a></li>
            <li class="<?php if( $current_page=='/about' ) echo 'active'; ?>"><a href="/about">About</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
	<div class="container">
     <?php if ( isset($content) ) echo $content; ?>
	</div>
	<footer class="footer">
      <div class="container">
        <p class="developed">Developed by Francis H. Suan	</p>
      </div>
    </footer>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
	<?php if ( isset($scripts) ) echo $scripts; ?>
  </body>
</html>