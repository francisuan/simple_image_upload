<?php if( count($images) == 0 ) { ?>
<div class="alert alert-info" role="alert">No Images uploaded yet. Click on the <strong>Upload Image</strong> button above to add.</div>
<?php } ?>
<?php foreach($images as $image) { ?>
<div class="image_post_container panel panel-primary">
	<div class="panel-heading"><?php echo $image->title; ?></div>
	<div class="panel-body">
		<div class="image_container">
			<a href="/pictures/<?php echo $image->id.'.'.$image->extension; ?>" class="thumbnail open_full_image"><img src="/pictures/thumbnails/<?php echo $image->id.'.'.$image->extension.'?timestamp='.time(); ?>" /></a>
		</div>	
		<nav class="navbar navbar-default">
		  <div class="container-fluid">
			<div class="navbar-header navbar-right">
				<div href="#" class="navbar-brand"></div>
			</div>
			<div class="btn-group bavbar-btn" role="group">
				<button type="button" class="btn btn-success navbar-btn edit_image" image_id="<?php echo $image->id; ?>"><span class="glyphicon glyphicon-edit"></span> Edit</button>
				<button type="button" class="btn btn-danger navbar-btn delete_image" image_id="<?php echo $image->id; ?>"><span class="glyphicon glyphicon-remove"></span> Delete</button>
			</div>
		  </div>
		</nav>
	</div>
	<div class="panel-footer">
		<div class="file_name">File Name: <?php echo $image->file_name; ?></div>
		<div class="uploaded">Uploaded: <?php echo dateTime::createFromFormat('Y-m-d H:i:s',$image->created)->format('F j, Y, g:i a'); ?></div>
	</div>
</div>
<?php } ?>