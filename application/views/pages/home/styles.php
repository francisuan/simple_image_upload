.btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}
#image_upload_modal input[type=text] {
	width:100%;
}
div#preview_image, 
div#preview_image_edit 
{
	margin-bottom:8px;
}
div#preview_image>img, 
div#preview_image_edit>img 
{
	width:100%;
}
div#image_upload_modal input#title, 
div#image_edit_modal input#edit_title 
{
	text-align:center;
}
div#image_upload_modal label,
div#image_edit_modal label
{
	display:block;
	text-align:center;
}
div.image_post_container{
	margin-bottom:8px;
}
div.image_container{
	margin:5px 0;
}
div.image_post_container .image_container img{
	max-width:100%;
}
div.image_post_container .title,
div.image_edit_modal .edit_title
{
	text-align:center;
}
div.image_post_container .date_added{
	text-align:right;
}