<script type="text/javascript" src="/js/jquery.iframe-transport.js"></script>
<script type="text/javascript">
$('a#upload_image').click(function(e){
	//reset everything before showing the modal form
	$('div#preview_image').html('');
	$('div.image_description').hide();
	$('div.upload_button_container').hide();
	$('input#title').val('');
	$('#image_upload_modal').modal('show');
});
$('#image_upload_modal').on('hidden.bs.modal',function(e){
	refresh_page();
});
$('input#selected_image_file').change(function(){
	//show the currently selected file from the modal view
	var file_reader = new FileReader();
	
	file_reader.onload = function (e) {
		//create a new image object
		var image = $(new Image()).attr('src', e.target.result);
		//append to the preview image div on the modal
		$('div#preview_image').html(image);
	}
	
	file_reader.readAsDataURL(this.files[0]);
	
	//show additional input fields
	
	$('div.image_description').show();
	$('div.upload_button_container').show();
	
	//focus the user to the title input
	
	$('input#title').focus();
	
});
$('input#selected_image_file_edit').change(function(){
	//show the currently selected file from the modal view
	var file_reader = new FileReader();
	
	file_reader.onload = function (e) {
		//create a new image object
		var image = $(new Image()).attr('src', e.target.result);
		//append to the preview image div on the modal
		$('div#preview_image_edit').html(image);
	}
	
	file_reader.readAsDataURL(this.files[0]);
	
	//show additional input fields
	
	$('div.image_description').show();
	$('div.upload_button_container').show();
	
	//focus the user to the title input
	
	$('input#title').focus();
	
});
$('button#submit_image').click(function(e){
	e.preventDefault();
	//via ajax, post to the server this file, while showing a posting message
	$.ajax({
		url : '/image/post',
		data : {
			title : $('input#title').val(),
		},
		files : $('input#selected_image_file'),
		iframe : true,
		dataType : 'json',
		success : function (data) {
			console.log(data);
			if ( data.saved ){
				//close the modal form
				$('#image_upload_modal').modal('hide');
				display_alert('File Uploaded.','success');
				//refresh the pages
				refresh_page();
			}
		},
	});
});
$('button#save_edits').click(function(e){
	e.preventDefault();
	//via ajax, post to the server this file, while showing a posting message
	$.ajax({
		url : '/image/post_edit',
		data : {
			image_id : $('input#edit_image_id').val(),
			title : $('input#edit_title').val(),
		},
		files : $('input#selected_image_file_edit'),
		iframe : true,
		dataType : 'json',
		success : function (data) {
			console.log(data);
			if ( data.saved ){
				//close the modal form
				$('#image_edit_modal').modal('hide');
				display_alert('Item Updated.','success');
				//refresh the pages
				refresh_page();
			}
		},
	});
});
$('body').on('keydown', '#title', function(e) {
    if (e.which == 9) {
        e.preventDefault();
        // do your code
		$('button#submit_image').focus();
    }
});
$("a.open_full_image").on("click", function(e) {
  e.preventDefault();
  var src = $(this).attr('href');
  $('#image_full_screen_modal div.modal-body').html('<div><img src="'+src+'" style="width:100%;" /></div><div><a href="'+src+'" target="_blank">Download</a></div>');
  $('#image_full_screen_modal').modal('toggle');
});
assign_buttons();
function refresh_page()
{
	//retrieve new posts from server via ajax
	$.ajax({
		url:'/pages/new_posts',
	}).done(function(body) {
		$('div.new_posts').html(body);
		assign_buttons();
	});
}
function assign_buttons()
{
	$('button.delete_image').click(function(){
		if ( confirm('Are you sure you want to delete this image?') )
		{
			var image_id = $(this).attr('image_id');
			$.ajax({
				url : '/image/delete',
				type : 'POST',
				data : {
					image_id : image_id
				},
				dataType : 'json',
				success : function (data) 
				{
					if( data.saved )
					{
						display_alert('Item Deleted.','success');
						refresh_page();
					}
				}
			});	
		}		
	});
	$('button.edit_image').click(function(){
		var image_id = $(this).attr('image_id');
		//clear first the inputs
		$('#edit_image_id').val('');
		$('#edit_title').val('');
		$('#current_image>a>img').attr('src','');
		$('div#preview_image_edit').html('');
		$('#selected_image_file_edit').val('');
		$.ajax({
			url : '/image/edit',
			type : 'POST',
			data : {
				image_id : image_id
			},
			dataType : 'json',
			success : function (data)
			{
				//set the data to the form
				$('#edit_image_id').val(data.id);
				$('#edit_title').val(data.title);				
				$('#current_image>a>img').attr('src','/pictures/thumbnails/'+data.id+'.'+data.extension+'?timestamp='+new Date().getTime());
				//display the edit modal form
				$('#image_edit_modal').modal('toggle');
			}
		});	
	});
}
function display_alert(message,type)
{
	if ( typeof type == 'undefined' ) type = 'info';
	$('#alerts_container').html('<div class="alert alert-'+type+'"><strong>Success!</strong> '+message+'</div>');
}
</script>