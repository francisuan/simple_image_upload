<h1 class="center">Welcome to Francis' Simple Image Upload</h1>
<p class="center"><a id="upload_image" class="btn btn-primary"><span class="glyphicon glyphicon-cloud-upload"></span> Upload Image</a></p>
<div id="alerts_container" class=""></div>
<div class="new_posts"><?php if ( isset($new_posts) ) echo $new_posts; ?></div>
<!-- Modals -->
<div class="modal fade" id="image_upload_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Upload an image</h4>
      </div>
      <div class="modal-body">
		<?php echo Form::open( NULL, array('id'=>'image_form') ); ?>
		<div class="image_description" style="display:none;">
			<div class="input text">				
				<input type="text" name="title" id="title" class="form-control" placeholder="Title">
				<label>Give your image a title</label>
			</div>
		</div>
		<div id="preview_image"></div>		
		<p class="center">
        <label class="btn btn-default btn-file">
			Browse <input id="selected_image_file" type="file" name="selected_image_file" accept="image/x-png,image/gif,image/jpeg" hidden>
		</label>
		</p>
		<div class="upload_button_container" style="display:none;">
			<p class="center"><button id="submit_image" class="btn btn-primary" type="submit">Submit</button></p>
		</div>
		<?php echo Form::close(); ?>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="image_full_screen_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-body">
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="image_edit_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit</h4>
      </div>
      <div class="modal-body">
		<div class="input text">				
			<input type="text" name="edit_title" id="edit_title" class="form-control" placeholder="Title">
			<label>Image Title</label>
		</div>
		<div>
			<div id="current_image"><a href="#" class="thumbnail"><img src="" /></a></div>
			<label>Current Image</label>
		</div>
		<div id="preview_image_edit"></div>	
		<p class="center">
        <label class="btn btn-default btn-file">
			Select an Image to Replace <input id="selected_image_file_edit" type="file" name="selected_image_file_edit" accept="image/x-png,image/gif,image/jpeg" hidden>
		</label>
		</p>
		<input type="hidden" name="edit_image_id" id="edit_image_id" />
		<p class="center"><button id="save_edits" class="btn btn-primary" type="submit">Save</button></p>
      </div>
    </div>
  </div>
</div>