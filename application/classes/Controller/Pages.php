<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Pages extends Controller_Template {
	
	public $template = 'layouts/default';
	
	public function action_index()
	{
		try
		{
			//retrive images
			$images = ORM::factory('Image')
				->order_by('id','desc')
				->find_all();
		}
		catch(Exception $e)
		{
			echo $e;
		}
		$this->template->title = 'Simple Image Upload';
		$this->template->content = View::factory('pages/home')
			->set('new_posts',View::factory('pages/new_posts')
				->set('images',$images)
			
			);
		$this->template->scripts = View::factory('pages/home/scripts');
		$this->template->extra_css = View::factory('pages/home/styles');
		
	}

	public function action_about()
	{
		$this->template->title = 'About';
		$this->template->content = View::factory('pages/about');
	}
	
	public function action_new_posts()
	{
		$images = ORM::factory('Image')
			->order_by('id','desc')
			->find_all();
		//don't render the template as request to this will be from ajax
		$this->template = View::factory('layouts/ajax');
		$this->template->content = View::factory('pages/new_posts')
			->set('images',$images);
	}

} // End Pages
