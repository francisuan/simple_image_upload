<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Image extends Controller {
	public function action_post()
	{
		//initialize request
		$request = Request::factory();
		
		//check if request is ajax
		//if ( $this->request->is_ajax() == false ) die('invalid access');

		//get post data
		$title = Arr::get($_POST,'title',NULL);
		$file = Arr::get($_FILES,'selected_image_file',NULL);
		
		//extract first the file extension
		$file_name = $file['name'];
		$file_name_array = explode('.',$file_name);
		$extension = end($file_name_array);
		
		//instance the Image model
		$image = ORM::factory('Image');
		//save first the file title, size etc to database
		$image->title = $title;
		$image->file_name = $file['name'];
		$image->size = $file['size'];
		$image->extension = $extension;
		$image->created = date('Y-m-d H:i:s');
		try
		{
			$image->save();
		}
		catch(ORM_Validation_Exception $e)
		{
			die('Oops! '.$e);
		}
		
		//get the new added id
		$new_image_id = $image->id;
		
		//move the uploaded file to the accessible directory and use file id
		$upload_location = DOCROOT.'pictures'.DS;
		try
		{			
			$file_name = $new_image_id.'.'.$extension;
			$image_file = $upload_location.$file_name;
			move_uploaded_file($file['tmp_name'],$image_file);
		}
		catch(Exception $e)
		{			
			die('Oops! '.$e);
		}
		
		try
		{
			//after upload, create thumbnails
			Image::factory($image_file)
				->resize(128, 149, Image::PRECISE)
				->crop(128, 149)
				->save($upload_location.'thumbnails'.DS.$file_name);
		}
		catch(Exception $e)
		{
			die('Oops! '.$e);
		}
		
		echo json_encode(array('saved'=>true));
	}
	
	public function action_delete()
	{
		$image_id = Arr::get($_POST,'image_id',NULL);
		//delete this image
		$image = ORM::factory('Image',$image_id);
		//delete the image file
		try
		{
			$upload_location = DOCROOT.'pictures'.DS;
			//delete the large file
			if (file_exists($upload_location.$image->id.'.'.$image->extension)==true)
			{
				unlink($upload_location.$image->id.'.'.$image->extension);
			}
			//delete the thumbnail
			if (file_exists($upload_location.'thumbnails'.DS.$image->id.'.'.$image->extension))
			{
				unlink($upload_location.'thumbnails'.DS.$image->id.'.'.$image->extension);
			}
		}
		catch(Exception $e)
		{
			die('Oops! '.$e);
		}
		try
		{						
			//delete the database record
			$image->delete();
		}
		catch(Exception $e)
		{
			die('Oops! '.$e);
		}
		
		echo json_encode(array('saved'=>true));
	}
	
	public function action_edit()
	{
		$image_id = Arr::get($_POST,'image_id',NULL);
		$image = ORM::factory('Image',$image_id);
		$data = array(
			'id'=>$image->id,
			'title'=>$image->title,
			'file_name'=>$image->file_name,
			'extension'=>$image->extension,
		);
		echo json_encode($data);
	}
	
	public function action_post_edit()
	{
		$image_id = Arr::get($_POST,'image_id',NULL);
		$image = ORM::factory('Image',$image_id);
		
		//get the values that need saving
		$new_title = Arr::get($_POST,'title',NULL);
		if( empty($new_title) == false )
		{
			$image->title = $new_title;
		}
		
		$file = Arr::get($_FILES,'selected_image_file_edit',NULL);
		
		if ( $file['size']>0 )
		{
			//a file was selected to replace current one
			//extract first the file extension
			$file_name = $file['name'];
			$file_name_array = explode('.',$file_name);
			$extension = end($file_name_array);
			
			//update file related records
			$image->file_name = $file['name'];
			$image->size = $file['size'];
			$image->extension = $extension;
			$image->created = date('Y-m-d H:i:s');
			
			$upload_location = DOCROOT.'pictures'.DS;
			
			try
			{			
				$file_name = $image_id.'.'.$extension;
				$image_file = $upload_location.$file_name;
				move_uploaded_file($file['tmp_name'],$image_file);
			}
			catch(Exception $e)
			{			
				die('Oops! '.$e);
			}
			
			try
			{
				//after upload, create thumbnails
				Image::factory($image_file)
					->resize(128, 149, Image::PRECISE)
					->crop(128, 149)
					->save($upload_location.'thumbnails'.DS.$file_name);
			}
			catch(Exception $e)
			{
				die('Oops! '.$e);
			}
		}
		
		
		//now that everything is done, save
		$image->save();
		
		echo json_encode(array('saved'=>true));
	}

} // End Image
