# Developer's Documentation by Francis H. Suan

Developed From the Following Environment
	Operating System: Windows 10 64bit
	Apache Version 2.4.18
	Php Version 5.6.19
	MySql Version 5.7.11

## SETUP

	1. This application requires a working database connection. database configuration file is located at 'modules\database\config.php'. You may modify as necessary but current application's database settings are:
		'hostname'   => 'localhost',
		'database'   => 'simple_image_upload',
		'username'   => 'simple_image_upload',
		'password'   => 'HAa2F7P33rqUTB8m',
	
	2. After database has been created, table 'images' should be created. Login to mysql using phpMyAdmin using the username and password then import the file '\application\config\schema\images.sql' onto phpmyadmin
	
	3. Application css, scripts and styles using bootstrap work best when run relative to a domain instead of within a folder, ex: http://simple_image_upload/. Enable virtual hosts for wamp or xamp
		
		1. First, On Wamp, for example, edit apache's httpd.conf file located in \wamp64\bin\apache<version>\conf\httpd.conf directory and uncomment the line below:
			Include conf/extra/httpd-vhosts.conf
		
		2. Create a virtual host by adding the folling lines on the httpd.conf file
			<VirtualHost *:80>
				ServerName simple_image_upload
				DocumentRoot C:/wamp64/www/simple_image_upload/webroot
				<Directory  "C:/wamp64/www/simple_image_upload/webroot">
					Options +Indexes +FollowSymLinks +MultiViews
					AllowOverride All
					Require local
				</Directory>
			</VirtualHost>
		
		3. Add a new hosts record to windows located at C:\Windows\System32\drivers\etc\hosts, to tell windows that address simple_image_upload is located locally.
			127.0.0.1			simple_image_upload
		
	4. Finally, start or restart wamp to save settings then on a browser, type in http://simple_image_upload/
		
		